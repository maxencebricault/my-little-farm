import React, { useState } from 'react';
import './Field.css';
import { IField, TIME_TO_HARVEST } from "../../App";

function Field(props: { field: IField, harvest: (event: React.MouseEvent<HTMLDivElement>) => void }) {
  const [ isReadyToHarvest, setIsReadyToHarvest ] = useState<boolean>(false);

  setTimeout(() => setIsReadyToHarvest(true), TIME_TO_HARVEST);

  return (
    <div className={`Field ${isReadyToHarvest ? 'isReadyToHarvest' : ''}`} onClick={props.harvest}
         data-id={props.field.id}>
      <span>{props.field.id}</span>
    </div>
  );
}

export default Field;
