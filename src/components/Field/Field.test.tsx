import React from 'react';
import { render, screen } from '@testing-library/react';
import Field from './Field';
import { IField, TIME_TO_HARVEST } from "../../App";

test('Display a new field', () => {
  const field = { id: Date.now(), quantity: 10 } as IField;
  const mockHarvest = (event: React.MouseEvent<HTMLDivElement>) => {
  }
  render(<Field field={field} harvest={mockHarvest}/>);
  const linkElement = screen.getByText(field.id);
  // Vérifier que le champ est bien créé et que l'id est présent dans le document

});

test('Add harvestable class after X milliseconds', () => {
  const field = { id: Date.now(), quantity: 10 } as IField;
  const mockHarvest = (event: React.MouseEvent<HTMLDivElement>) => {}
  const { container } =  render(<Field field={field} harvest={mockHarvest}/>);

  // Tester que la classe est bien ajoutée au bout de TIME_TO_HARVEST seconds (constante
});
