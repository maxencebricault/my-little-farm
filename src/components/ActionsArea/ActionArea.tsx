import React from 'react';

function ActionArea(props: {quantity: number}) {

  return (
    <div>
      Total quantity : <strong>{props.quantity}</strong> Kgs  🌽
    </div>
  );
}

export default ActionArea;
