import React, { useCallback, useEffect, useState } from 'react';
import './App.css';
import Field from "./components/Field/Field";
import ActionArea from "./components/ActionsArea/ActionArea";

export interface IField {
  id: number;
  quantity: number;
  type?: string
}

const MAX_FIELDS = 9;
export const TIME_TO_HARVEST = 2000

function App() {
  const [ fields, setFields ] = useState<IField[]>([]);
  const [ quantity, setQuantity ] = useState(0);

  // Ajouter un champ dans le state
  const addField = () => {
    if (fields.length < MAX_FIELDS) {
      const newField: IField = { id: Date.now(), quantity: 10, type: 'e' };
      setFields((prevState) => [ ...prevState, newField ]);
    }
  }

  // Récolter le champ clické
  const harvest = (event: React.MouseEvent<HTMLDivElement>) => {
    const id = ((event.target as HTMLDivElement).dataset.id);

    if (id) {
      // Find field
      const field = fields.find((elem: IField) => {
        return elem.id === +id;
      });

      // Add to global quantity then delete
      if (field) {
        setQuantity(quantity + field.quantity);

        deleteField(field);
      }
    }
  }

  const deleteField = (field: IField) => {
    const filteredFields = fields.filter((elem: IField) => {
      return elem.id !== field.id;
    })

    setFields(filteredFields);
  }

  const harvestAll = useCallback(() => {
    fields.forEach(field => {
      if (field.id + TIME_TO_HARVEST < Date.now()) {
        setQuantity(quantity + field.quantity)
        deleteField(field);
      }
    })
  },[deleteField, fields, quantity])

  return (
    <div className="App">
      <h1>My Farm</h1>
      <img src={process.env.PUBLIC_URL + 'farm.jpeg'} alt="Farm"/>
      <ActionArea quantity={quantity}/>
      <button onClick={() => addField()}>Add field</button>
      <br/>
      <button onClick={() => harvestAll()}>Harvest everything</button>

      <div className="fieldsZone">
        {fields.map((field, idx: number) => {
          return <Field field={field} harvest={harvest} key={idx}/>
        })}
      </div>
    </div>
  );
}

export default App;
